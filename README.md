# Slimmed communication_manager
This is a slimmed down version of the DDS communication_manager that may be run standalone without SubT's dependencies.
It simply converts ROS topics to DDS to send over the network. Then on a different robot, from DDS back to ROS.

The yaml file `config/params_dds.yaml` defines the list of topics to broadcast.

## How to Add to Your Project
Ideally, the cleanest way to add communication_manager to your project would be to make a fork for your project. 
Unfortunately however, this repo depends on Git LFS, and BitBucket does NOT transfer LFS files through forks on the GUI.
Therefore the fastest way is to make a branch off master within this same repo.

If you really prefer things to be clean in a separate fork, then you'll have to download the LFS objects then reupload it to your fork. 
See [here](https://support.atlassian.com/bitbucket-cloud/docs/current-limitations-for-git-lfs-with-bitbucket/) for more info on this workaround.

## How to Install

Tested on Ubuntu 18.04, ROS melodic. Requires CMake >= 3.7 to properly build with RTI's Connext DDS.

Make sure to have [Git LFS](https://git-lfs.github.com/) installed and setup. This repository uses LFS to point to Connext DDS 6.0.0.

```bash
cd ~/basestation_ws/src
git clone git@bitbucket.org:castacks/base_main_class.git  # dependency
git clone git@bitbucket.org:cmusubt/communication_manager.git
catkin config
catkin build communication_manager
```


## How to Setup

Setup your topics in the yaml file `config/params.yaml`.

The **same** yaml config file should be used on all network-interface computers. The different computers are differentiated by the 'computer' roslaunch argument, which should match up with the yaml file -- see "how to run" below. The yaml file is interpreted differently based on this 'computer' argument.


## How to Run

<!-- On basestation:
```bash
roslaunch communication_manager communication_manager.launch computer:=basestation
``` 
-->

On robot1:
```bash
roslaunch communication_manager communication_manager.launch computer:=uav1
```

On robot2:
```bash
roslaunch communication_manager communication_manager.launch computer:=uav2
```

On robot2:
```bash
roslaunch communication_manager communication_manager.launch computer:=uav3
```

## Documentation
See `docs/` for additional documentation.

## Who to Contact
- Andrew Jong - Maintains the slimmed version (this one)
- Bill Drozd - Wrote the DDS version
- Ryan Darnley - Maintains the DDS version for SubT
- Grame Best - Wrote the original non-DDS version