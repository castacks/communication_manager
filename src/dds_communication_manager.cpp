#include <communication_manager/dds_communications_manager.h>



/**
 * @brief This is necessary for BaseNode but should be changed to return a unique_ptr.
 * 
 * @return BaseNode* 
 */
    BaseNode* BaseNode::get(){
    subt::comms::DDSCommsManager* dds_node = new subt::comms::DDSCommsManager();
    return dds_node;
}


int main(int argc, char** argv)
{
    ros::init(argc, argv, "dds_comms_node");
    ROS_INFO_STREAM(" STARTING COMMS Manager");

    ros::NodeHandle node_handle("~");
    ROS_INFO_STREAM(" GOT NH");

    std::string config_xml;
    if(!node_handle.getParam("config_xml", config_xml)) {
        ROS_INFO_STREAM("CONFIG XML NOT FOUND! ");
        return 1;
    }
    ROS_INFO_STREAM(" LOADING CONFIG :" <<  config_xml);
    
    rti::core::QosProviderParams provider_params;
    provider_params.url_profile(dds::core::StringSeq(1, config_xml));
    dds::core::QosProvider::Default()->default_provider_params(provider_params);

    subt::comms::DDSCommsManager ddsManager;
    ROS_INFO_STREAM(" RUNNING " <<  config_xml);

    ddsManager.Run();

    return 0;
}