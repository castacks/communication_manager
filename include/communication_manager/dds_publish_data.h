#ifndef DDS_PUBLISH_DATA_H
#define DDS_PUBLISH_DATA_H

#include <any>
#include "ros/ros.h"
#include <dds/core/policy/CorePolicy.hpp>
#include <communication_manager/subt_comms_factory.h>
#include <communication_manager/queueInfo.h>

namespace subt
{

namespace comms
{

/**
 * @brief 
 * 
 */

template <class R, class T>
class PublisherData
{

public:
    PublisherData(ros::NodeHandle &_nh, const std::string &_transmission_profile, const std::string &_rosTopic, const std::string &_ddsTopic, dds::pub::qos::DataWriterQos &writer_qos, dds::topic::Topic<T> &check_topic, dds::domain::DomainParticipant &_participant, dds::pub::Publisher &_ddsPublish, const uint32_t &_filterCount = 0) : control_logic(false), control_status(true), nh(_nh), rosTopic(_rosTopic), ddsTopic(_ddsTopic), ddsPublish(_ddsPublish), filterCount(_filterCount), filterIndex(0), transmission_profile(_transmission_profile), ddsTopicObj(check_topic), ddsWriterObj(ddsPublish, ddsTopicObj, writer_qos)
    {
        //, ddsWriterObj(_ddsPublish, ddsTopicObj){
        this->init_data_writer();
    }

    /**
         * @brief Construct a new Publisher Data object This object creates DDS Data Writers for a variety of different ROS/DDS topics. 
         * 
         * @param participant 
         */

    PublisherData(ros::NodeHandle &_nh, const std::string &_transmission_profile, const std::string &_rosTopic, const std::string &_ddsTopic, dds::pub::qos::DataWriterQos &writer_qos, dds::domain::DomainParticipant &_participant, dds::pub::Publisher &_ddsPublish, const uint32_t &_filterCount = 0, const bool _control_logic = false) : control_logic(_control_logic), control_status(true), nh(_nh), rosTopic(_rosTopic), ddsTopic(_ddsTopic), ddsPublish(_ddsPublish), filterCount(_filterCount), filterIndex(0), transmission_profile(_transmission_profile), ddsTopicObj(_participant, ddsTopic), ddsWriterObj(ddsPublish, ddsTopicObj, writer_qos)
    {
        /* , ddsWriterObj(_ddsPublish, ddsTopicObj)*/

        /* PublisherData(ros::NodeHandle& _nh, const std::string transmission_profile, const std::string& _rosTopic, const std::string& _ddsTopic, 
        dds::domain::DomainParticipant& _participant, dds::pub::Publisher& _ddsPublish, const uint32_t& _filterCount = 0, const int queueSize=1000) : rosTopic(_rosTopic), 
        ddsTopic(_ddsTopic), ddsTopicObj(_participant, ddsTopic), filterCount(_filterCount), filterIndex(0) {  
        
            //ddsTopicObj(_participant, ddsTopic), 
        
            //ddsWriterObj(_ddsPublish, ddsTopicObj)
            //writer_qos = dds::core::QosProvider::datawriter_qos(transmission_profile);
            //
            */
        //ddsWriterObj = std::make_shared<dds::pub::DataWriter<T>>(_ddsPublish, ddsTopicObj);

        this->init_data_writer();
    }

    //template <class R, class T>
    void rosTopicCallback(R rosMsg)
    {
        //ROS_INFO_STREAM("ROS CALLBACK!  " << rosTopic);
        //uint32_t serial_size = ros::serialization::serializationLength(rosMsg);

        if (control_status)
        {
            if (filterIndex == 0 || filterCount == 0)
            {
                T ddsMsg = subt::comms::SerializationHelper::convertMsg(rosMsg);
                //ROS_INFO_STREAM("DDS writing msg bytes " << serial_size);

                ddsWriterObj->write(ddsMsg);

		int cacheStatus = ddsWriterObj->datawriter_cache_status().sample_count();
		//std_msgs::Int32 rosCacheStatus;
		//rosCacheStatus.data = cacheStatus;
		//ddsQueuePublisher.publish(rosCacheStatus);

		std::string topicName = ddsWriterObj.topic().name();
		//ddsNamePublisher.publish(topicName);

		communication_manager::queueInfo info;
		info.topicName = topicName;
		info.queueSize = cacheStatus;
		info.bw = bw;
		info.delay = delay;

		ddsQueueInfoPublisher.publish(info);


            }

            if (filterCount > 0)
            {
                filterIndex++;
            }

            if (filterIndex == filterCount)
            {
                filterIndex = 0;
            }
        }
    }

    void delayCallback(const std_msgs::String::ConstPtr& msg)
    {
	    delay = msg->data;
    }

    void bwCallback(const std_msgs::String::ConstPtr& msg)
    {
	    bw = msg->data;
    }

private:
    void init_data_writer()
    {

        //pass it in instead
        //writer_qos = dds::core::QosProvider::Default().datawriter_qos();

        /*writer_qos << rti::core::policy::PublishMode::Asynchronous();
           //writer_qos << dds::core::policy::History::KeepAll();
               
           if (transmission_profile.find("reliable") != std::string::npos){
                 writer_qos << dds::core::policy::Reliability::Reliable();
                 
                if (transmission_profile.find("last") != std::string::npos){
                 writer_qos << dds::core::policy::History::KeepAll();
                } else {
                 writer_qos << dds::core::policy::History::KeepLast(5);
                } 

            } else {
                 writer_qos << dds::core::policy::Reliability::BestEffort();
            } */

        //ddsWriterObj = new dds::pub::DataWriter<T>>(ddsPublish, ddsTopicObj, writer_qos);

        std::string ddsFixedTopic;
        if (ddsTopic.empty())
        {
            ///If you don't provide a DDS Topic, then just use something similar to the ros topic name
            ddsFixedTopic = rosTopic;
        }
        else
        {
            ddsFixedTopic = ddsTopic;
        }
        ///No slashes allowed in DDS Topic names...so convert these naively....
        std::replace(ddsFixedTopic.begin(), ddsFixedTopic.end(), '/', '_');

        this->rosSubscriber = nh.subscribe(rosTopic, 1000, &PublisherData::rosTopicCallback, this);
        ROS_INFO_STREAM("ROS (" << rosTopic << ") -> DDS (" << ddsFixedTopic << ")"
                                << " filter: " << filterCount);

	    this->delaySubscriber = nh.subscribe("/delay", 100, &PublisherData::delayCallback, this);
	    this->bwSubscriber = nh.subscribe("/bw", 100, &PublisherData::bwCallback, this);

        ROS_INFO_STREAM("OPEN TOPIC!");
    }

    std::string ddsTopic;
    std::string rosTopic;
    std::string typeCode;
    //const std::string initYAML;

    uint32_t filterIndex;
    uint32_t filterCount;
    bool control_logic;
    bool control_status;

    ///ros::NodeHandle nh;
    bool sweepTopic;
    dds::pub::Publisher ddsPublish;
    //auto testCallback;
    dds::topic::Topic<T> ddsTopicObj;
    ros::NodeHandle nh;
    dds::pub::qos::DataWriterQos writer_qos;
    dds::pub::DataWriter<T> ddsWriterObj;
    std::string transmission_profile;

    std::vector<std::any> anyVectorCheck;

    //This is the ID of the robot....it is hard-coded for now.
    std::string entityName;

    //This is a list of all robot prefixes.....is this even needed?
    std::vector<std::string> robotPrefixes;

    std::map<std::string, dds::pub::AnyDataWriter> writerInfo;

    ros::Subscriber rosSubscriber;
    ros::Subscriber controlSubscriber;

    ros::Publisher ddsQueueInfoPublisher = nh.advertise<communication_manager::queueInfo>("/dds_queue_info", 10);
    ros::Subscriber delaySubscriber;
    ros::Subscriber bwSubscriber;
    std::string bw;
    std::string delay;
};

} // namespace comms

} // namespace subt

#endif
