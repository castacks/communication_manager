#ifndef SUBT_COMMS_FACTORY_H
#define SUBT_COMMS_FACTORY_H
#include <ros/ros.h>
#include <iostream> 
#include <stdio.h>
#include <stdlib.h>
#ifdef RTI_VX653
    #include <vThreadsData.h>
#endif

#include <dds/sub/ddssub.hpp>
#include <dds/domain/ddsdomain.hpp>
#include <dds/core/ddscore.hpp>
#include <dds/topic/ddstopic.hpp>
#include <dds/pub/ddspub.hpp>
#include <rti/core/ListenerBinder.hpp>

#include "geometry_msgs/msg/PointStamped.hpp"
#include "geometry_msgs/msg/PointStampedPlugin.hpp"
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/Point.h>
#include "geometry_msgs/msg/PolygonStamped.hpp"
#include "geometry_msgs/msg/PolygonStampedPlugin.hpp"
#include <geometry_msgs/PolygonStamped.h>
#include "geometry_msgs/msg/PolygonStamped.hpp"
#include <geometry_msgs/Polygon.h>
#include "geometry_msgs/msg/Polygon.hpp"

#include "geometry_msgs/msg/Point32.hpp"
#include "geometry_msgs/msg/Point32Plugin.hpp"

#include "geometry_msgs/msg/Point.hpp"
#include "geometry_msgs/msg/PointPlugin.hpp"


#include "sensor_msgs/msg/PointCloud2.hpp"
#include "sensor_msgs/msg/PointField.hpp"

#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/PointField.h>

#include "std_msgs/msg/Bool.hpp"
#include <std_msgs/Bool.h>

#include "std_msgs/msg/Time.hpp"
#include <std_msgs/Time.h>


#include "std_msgs/msg/Float32.hpp"
#include <std_msgs/Float32.h>

#include "std_msgs/msg/Int32.hpp"
#include <std_msgs/Int32.h>


#include "std_msgs/msg/Int32MultiArray.hpp"
#include <std_msgs/Int32MultiArray.h>

#include "std_msgs/msg/ByteMultiArray.hpp"
#include <std_msgs/ByteMultiArray.h>

#include "std_msgs/msg/MultiArrayDimension.hpp"
#include <std_msgs/MultiArrayDimension.h>

#include "diagnostic_msgs/msg/KeyValue.hpp"
#include <diagnostic_msgs/KeyValue.h>

#include "diagnostic_msgs/msg/DiagnosticStatus.hpp"
#include <diagnostic_msgs/DiagnosticStatus.h>


#include "diagnostic_msgs/msg/DiagnosticArray.hpp"
#include <diagnostic_msgs/DiagnosticArray.h>

#include "std_msgs/msg/String.hpp"
#include <std_msgs/String.h>



#include "std_msgs/msg/ColorRGBA.hpp"
#include <std_msgs/ColorRGBA.h>


#include "std_msgs/msg/Duration.hpp"
#include <std_msgs/Duration.h>

#include <geometry_msgs/Transform.h>
#include "geometry_msgs/msg/Transform.hpp"

#include <geometry_msgs/PoseWithCovariance.h>
#include "geometry_msgs/msg/PoseWithCovariance.hpp"

#include <geometry_msgs/PoseStamped.h>
#include "geometry_msgs/msg/PoseStamped.hpp"

#include <geometry_msgs/TwistWithCovariance.h>
#include "geometry_msgs/msg/TwistWithCovariance.hpp"

#include <geometry_msgs/Twist.h>
#include "geometry_msgs/msg/Twist.hpp"


#include <geometry_msgs/Vector3.h>
#include "geometry_msgs/msg/Vector3.hpp"

#include <nav_msgs/Odometry.h>
#include "nav_msgs/msg/Odometry.hpp"


#include <visualization_msgs/Marker.h>
#include "visualization_msgs/msg/Marker.hpp"

#include <visualization_msgs/MarkerArray.h>
#include "visualization_msgs/msg/MarkerArray.hpp"


#include <sensor_msgs/Joy.h>
#include "sensor_msgs/msg/Joy.hpp"

#include <sensor_msgs/Image.h>
#include "sensor_msgs/msg/Image.hpp"

#include <sensor_msgs/CompressedImage.h>
#include "sensor_msgs/msg/CompressedImage.hpp"

#include "std_msgs/msg/String.hpp"
#include <std_msgs/String.h>


//// Changes for Asynchronous_Publication
// For timekeeping
#include <time.h>
#include <string>
#include <memory>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>


namespace subt {
    namespace comms {

        bool file_exists (const std::string& name) {
            struct stat buffer;   
            return (stat (name.c_str(), &buffer) == 0); 
        }


        
        /**
         * @brief This is a class used to actual map data between ROS <---> DDS types. This is implemented using generated implementation as below instead of using RTTI or DDS Dynamic Types on purpose:
         * 
         * 1) To induce compile time failures via Templates over Run-Time failures in the event that some tries to send an unsupported type.
         * 
         * 2) Simplicity in debugging and maximizing performance (since we are directly setting struct members vs. recusively matching types (string compares yuck!) like a slicker RTTI like approach would do.
         * 
         * 3) Discourage people from sending new data types over the wire and instead making it much easier to use existing types rather than adding a new one. This is because a highly tuned system might entail some
         * other work when adding a new type beyond serialization such as QOS policy definition, prioritization vs. other data, latency/throughput.
         * 
         */
        class SerializationHelper{

        public:

            static void printBytes(std::vector<uint8_t> buffer){
                std::stringstream ssb;
                for (int i = 0; i < buffer.size(); i++ ){
                    ssb << ((int) buffer[i]) << ",";
                }
                ROS_INFO_STREAM("Array size = " << buffer.size());

                ROS_INFO_STREAM(ssb.str());
             } 

             static void printBytes(std::vector<char> buffer){
                std::stringstream ssb;
                
                for (int i = 0; i < buffer.size(); i++ ){
                    ssb << ((int) buffer[i]) << ",";
                }
                ROS_INFO_STREAM("Array size = " << buffer.size());
                ROS_INFO_STREAM(ssb.str());
             } 

            
            template<class R>
            static std::vector<uint8_t> rosMessageToBytes(R& rosMessage, bool debugPrint=false){
                size_t serial_size = ros::serialization::serializationLength(rosMessage);
                std::vector<uint8_t> buffer(serial_size);
                ros::serialization::OStream stream(buffer.data(), buffer.size());
                ros::serialization::serialize(stream, rosMessage);
                if (debugPrint){
                    ROS_INFO_STREAM(rosMessage);
                    printBytes(buffer);
                }

                return buffer;
             }

            template<class T>
            static std::vector<char> ddsMessageToBytes(T& ddsMessage, bool debugPrint=false){
                std::vector<char> buffer;
                rti::topic::to_cdr_buffer<T>(buffer, ddsMessage);
                size_t serial_size = buffer.size();            
                if (debugPrint){
                    ROS_INFO_STREAM(ddsMessage);
                    printBytes(buffer);
                }

                return buffer;
             }

            static std_msgs::msg::Header convertMsg(const std_msgs::Header& rosMessage){
                std_msgs::msg::Header outputMsg;
                std_msgs::msg::Time timeDat(rosMessage.stamp.sec, rosMessage.stamp.nsec);
                outputMsg.frame_id(rosMessage.frame_id);
                outputMsg.stamp(timeDat);
                return outputMsg;
            }

            static std_msgs::Header convertMsg(const std_msgs::msg::Header& ddsMessage){                
                std_msgs::Header rosHeader;
                rosHeader.stamp.sec = ddsMessage.stamp().sec();
                rosHeader.stamp.nsec = ddsMessage.stamp().nanosec();
                rosHeader.frame_id = ddsMessage.frame_id();
                return rosHeader;
            }


            static diagnostic_msgs::msg::KeyValue convertMsg(const diagnostic_msgs::KeyValue& inputMsg){
                diagnostic_msgs::msg::KeyValue outputMsg;
                outputMsg.key(inputMsg.key);
                outputMsg.value(inputMsg.value);
                return outputMsg;
            }

            static diagnostic_msgs::KeyValue convertMsg(const diagnostic_msgs::msg::KeyValue& inputMsg){
                diagnostic_msgs::KeyValue rosHeader;
                rosHeader.key = inputMsg.key();
                rosHeader.value = inputMsg.value();
                return rosHeader;
            }

            static diagnostic_msgs::msg::DiagnosticStatus convertMsg(const diagnostic_msgs::DiagnosticStatus& inputMsg){
                diagnostic_msgs::msg::DiagnosticStatus outputMsg;
                outputMsg.name(inputMsg.name);
		outputMsg.level(inputMsg.level);
                outputMsg.hardware_id(inputMsg.hardware_id);
                outputMsg.message(inputMsg.message);
                std::transform(inputMsg.values.begin(),inputMsg.values.end(),
                std::back_inserter(outputMsg.values()), [&](auto input){return subt::comms::SerializationHelper::convertMsg(input);} );

                return outputMsg;
            }

            static diagnostic_msgs::DiagnosticStatus convertMsg(const diagnostic_msgs::msg::DiagnosticStatus& inputMsg){
                diagnostic_msgs::DiagnosticStatus outputMsg;
                outputMsg.name = inputMsg.name();
                outputMsg.hardware_id = inputMsg.hardware_id();
		outputMsg.level = inputMsg.level();
                outputMsg.message = inputMsg.message();
                std::transform(inputMsg.values().begin(),inputMsg.values().end(),
                std::back_inserter(outputMsg.values), [&](auto input){return subt::comms::SerializationHelper::convertMsg(input);} );

                return outputMsg;
            }


            static diagnostic_msgs::msg::DiagnosticArray convertMsg(const diagnostic_msgs::DiagnosticArray& inputMsg){
                diagnostic_msgs::msg::DiagnosticArray outputMsg;
                outputMsg.header(subt::comms::SerializationHelper::convertMsg(inputMsg.header));
                std::transform(inputMsg.status.begin(),inputMsg.status.end(),
                std::back_inserter(outputMsg.status()), [&](auto input){return subt::comms::SerializationHelper::convertMsg(input);} );

                return outputMsg;
            }

            static diagnostic_msgs::DiagnosticArray convertMsg(const diagnostic_msgs::msg::DiagnosticArray& inputMsg){
                diagnostic_msgs::DiagnosticArray outputMsg;
                outputMsg.header = subt::comms::SerializationHelper::convertMsg(inputMsg.header());
                std::transform(inputMsg.status().begin(),inputMsg.status().end(),
                std::back_inserter(outputMsg.status), [&](auto input){return subt::comms::SerializationHelper::convertMsg(input);} );

                return outputMsg;
            }

            static std_msgs::msg::Bool convertMsg(const std_msgs::Bool& inputMsg){
                std_msgs::msg::Bool outputMsg;
                outputMsg.data(inputMsg.data);
                return outputMsg;
            }

            static std_msgs::Bool convertMsg(const std_msgs::msg::Bool& inputMsg){
                std_msgs::Bool rosHeader;
                rosHeader.data = inputMsg.data();
                return rosHeader;
            }

	  /*static std_msgs::msg::Time convertMsg(const std_msgs::Time& inputMsg){
		std_msgs::msg::Time outputMsg(inputMsg.data.sec, inputMsg.data.nsec);
            
                return outputMsg;
            }

            static std_msgs::Time convertMsg(const std_msgs::msg::Time& inputMsg){
                std_msgs::Time rosHeader;
		rosHeader.data.sec = inputMsg.sec();
                rosHeader.data.nsec = inputMsg.nanosec();		
                return rosHeader;
	   }*/


            static std_msgs::msg::Float32 convertMsg(const std_msgs::Float32& inputMsg){
                std_msgs::msg::Float32 outputMsg;
                outputMsg.data(inputMsg.data);
                return outputMsg;
            }

            static std_msgs::Float32 convertMsg(const std_msgs::msg::Float32& inputMsg){
                std_msgs::Float32 rosHeader;
                rosHeader.data = inputMsg.data();
                return rosHeader;
            }

            static std_msgs::msg::Int32MultiArray convertMsg(const std_msgs::Int32MultiArray& inputMsg){
                std_msgs::msg::Int32MultiArray outputMsg;
                            
                std::transform(inputMsg.data.begin(),inputMsg.data.end(),
                std::back_inserter(outputMsg.data()), [&](auto input){return input;} );

                return outputMsg;
            }

            static std_msgs::Int32MultiArray convertMsg(const std_msgs::msg::Int32MultiArray& inputMsg){
                std_msgs::Int32MultiArray rosHeader;
                std::transform(inputMsg.data().begin(),inputMsg.data().end(),
                std::back_inserter(rosHeader.data), [&](auto input){return input;} );
            
                return rosHeader;
            }

            static std_msgs::msg::ByteMultiArray convertMsg(const std_msgs::ByteMultiArray& inputMsg){
                std_msgs::msg::ByteMultiArray outputMsg;

                std::transform(inputMsg.data.begin(), inputMsg.data.end(),
                std::back_inserter(outputMsg.data()), [&](auto input){return input;} );

                return outputMsg;
            }

            static std_msgs::ByteMultiArray convertMsg(const std_msgs::msg::ByteMultiArray& inputMsg){
                std_msgs::ByteMultiArray rosHeader;
                std::transform(inputMsg.data().begin(),inputMsg.data().end(),
                std::back_inserter(rosHeader.data), [&](auto input) {return input;} );

                return rosHeader;
            }

            static std_msgs::msg::MultiArrayDimension convertMsg(const std_msgs::MultiArrayDimension& inputMsg){
                std_msgs::msg::MultiArrayDimension outputMsg;
                outputMsg.label(inputMsg.label);
                outputMsg.size(inputMsg.size);
                outputMsg.stride(inputMsg.stride);
                return outputMsg;
            }

            static std_msgs::MultiArrayDimension convertMsg(const std_msgs::msg::MultiArrayDimension& inputMsg){
                std_msgs::MultiArrayDimension rosHeader;
                rosHeader.label = inputMsg.label();
                rosHeader.size = inputMsg.size();
                rosHeader.stride = inputMsg.stride();
                return rosHeader;
            }


            static std_msgs::msg::Int32 convertMsg(const std_msgs::Int32& inputMsg){
                std_msgs::msg::Int32 outputMsg;
                outputMsg.data(inputMsg.data);
                return outputMsg;
            }

            static std_msgs::Int32 convertMsg(const std_msgs::msg::Int32& inputMsg){
                std_msgs::Int32 rosHeader;
                rosHeader.data = inputMsg.data();
                return rosHeader;
            }


              static std_msgs::msg::String convertMsg(const std_msgs::String& inputMsg){
                std_msgs::msg::String outputMsg;
                outputMsg.data(inputMsg.data);
                return outputMsg;
            }

            static std_msgs::String convertMsg(const std_msgs::msg::String& inputMsg){
                std_msgs::String rosHeader;
                rosHeader.data = inputMsg.data();
                return rosHeader;
            }

            static std_msgs::msg::Duration convertMsg(const std_msgs::Duration& inputMsg){
                std_msgs::msg::Duration outputMsg;
                outputMsg.sec(inputMsg.data.sec);
                outputMsg.nanosec(inputMsg.data.nsec);

                return outputMsg;
            }

            static std_msgs::Duration convertMsg(const std_msgs::msg::Duration& inputMsg){
                std_msgs::Duration rosHeader;
                rosHeader.data.sec = inputMsg.sec();
                rosHeader.data.nsec = inputMsg.nanosec();

                return rosHeader;
            }

            static geometry_msgs::msg::Point convertMsg(const geometry_msgs::Point& inputMsg){
                geometry_msgs::msg::Point outputMsg = geometry_msgs::msg::Point();
                outputMsg.x(inputMsg.x);
                outputMsg.y(inputMsg.y);
                outputMsg.z(inputMsg.z);
                return outputMsg;
            }

            static geometry_msgs::Point convertMsg(const geometry_msgs::msg::Point& inputMsg){
                geometry_msgs::Point outputMsg;
                outputMsg.x = inputMsg.x();
                outputMsg.y = inputMsg.y();
                outputMsg.z = inputMsg.z();
                return outputMsg;
            }


            static geometry_msgs::msg::Quaternion convertMsg(const geometry_msgs::Quaternion& inputMsg){
                geometry_msgs::msg::Quaternion outputMsg = geometry_msgs::msg::Quaternion();
                outputMsg.x(inputMsg.x);
                outputMsg.y(inputMsg.y);
                outputMsg.z(inputMsg.z);
                outputMsg.w(inputMsg.w);

                return outputMsg;
            }

            static geometry_msgs::Quaternion convertMsg(const geometry_msgs::msg::Quaternion& inputMsg){
                geometry_msgs::Quaternion outputMsg;
                outputMsg.x = inputMsg.x();
                outputMsg.y = inputMsg.y();
                outputMsg.z = inputMsg.z();
                outputMsg.w = inputMsg.w();

                return outputMsg;
            }

            static geometry_msgs::msg::Transform convertMsg(const geometry_msgs::Transform& inputMsg){
                geometry_msgs::msg::Transform outputMsg = geometry_msgs::msg::Transform();                
                outputMsg.translation(subt::comms::SerializationHelper::convertMsg(inputMsg.translation));
                outputMsg.rotation(subt::comms::SerializationHelper::convertMsg(inputMsg.rotation));
                return outputMsg;
            }

            static geometry_msgs::Transform convertMsg(const geometry_msgs::msg::Transform& inputMsg){
                geometry_msgs::Transform outputMsg;
                outputMsg.translation = subt::comms::SerializationHelper::convertMsg(inputMsg.translation());
                outputMsg.rotation = subt::comms::SerializationHelper::convertMsg(inputMsg.rotation());
                return outputMsg;
            }

            static geometry_msgs::msg::Point32 convertMsg(const geometry_msgs::Point32& inputMsg){
                geometry_msgs::msg::Point32 outputMsg = geometry_msgs::msg::Point32();
                outputMsg.x(inputMsg.x);
                outputMsg.y(inputMsg.y);
                outputMsg.z(inputMsg.z);
                return outputMsg;
            }

            static geometry_msgs::Point32 convertMsg(const geometry_msgs::msg::Point32& inputMsg){
                geometry_msgs::Point32 outputMsg;
                outputMsg.x = inputMsg.x();
                outputMsg.y = inputMsg.y();
                outputMsg.z = inputMsg.z();
                return outputMsg;
            }

            static geometry_msgs::msg::Vector3 convertMsg(const geometry_msgs::Vector3& inputMsg){
                geometry_msgs::msg::Vector3 outputMsg = geometry_msgs::msg::Vector3();
                outputMsg.x(inputMsg.x);
                outputMsg.y(inputMsg.y);
                outputMsg.z(inputMsg.z);
                return outputMsg;
            }

            static geometry_msgs::Vector3 convertMsg(const geometry_msgs::msg::Vector3& inputMsg){
                geometry_msgs::Vector3 outputMsg;
                outputMsg.x = inputMsg.x();
                outputMsg.y = inputMsg.y();
                outputMsg.z = inputMsg.z();
                return outputMsg;
            }
	    
            static std_msgs::msg::ColorRGBA convertMsg(const std_msgs::ColorRGBA& inputMsg){
                std_msgs::msg::ColorRGBA outputMsg = std_msgs::msg::ColorRGBA();
                outputMsg.r(inputMsg.r);
                outputMsg.g(inputMsg.g);
                outputMsg.b(inputMsg.b);
                outputMsg.a(inputMsg.a);
                return outputMsg;
            }

            static std_msgs::ColorRGBA convertMsg(const std_msgs::msg::ColorRGBA& inputMsg){
                std_msgs::ColorRGBA outputMsg;
                outputMsg.r = inputMsg.r();
                outputMsg.g = inputMsg.g();
                outputMsg.b = inputMsg.b();
                outputMsg.a = inputMsg.a();

                return outputMsg;
            }


            static sensor_msgs::msg::PointField convertMsg(const sensor_msgs::PointField& inputMsg){
                sensor_msgs::msg::PointField outputMsg = sensor_msgs::msg::PointField();
                outputMsg.name(inputMsg.name);
                outputMsg.offset(inputMsg.offset);
                outputMsg.datatype(inputMsg.datatype);
                outputMsg.count(inputMsg.count);
                return outputMsg;
            }

            static sensor_msgs::PointField convertMsg(const sensor_msgs::msg::PointField& inputMsg){
                sensor_msgs::PointField outputMsg;
                outputMsg.name = inputMsg.name();
                outputMsg.offset = inputMsg.offset();
                outputMsg.datatype = inputMsg.datatype();
                outputMsg.count = inputMsg.count();
                return outputMsg;
            }

            static geometry_msgs::msg::Pose convertMsg(const geometry_msgs::Pose& inputMsg){
                geometry_msgs::msg::Pose outputMsg = geometry_msgs::msg::Pose();                
                outputMsg.position(subt::comms::SerializationHelper::convertMsg(inputMsg.position));
                outputMsg.orientation(subt::comms::SerializationHelper::convertMsg(inputMsg.orientation));
                return outputMsg;
            }

            static geometry_msgs::Pose convertMsg(const geometry_msgs::msg::Pose& inputMsg){
                geometry_msgs::Pose outputMsg;
                outputMsg.position = subt::comms::SerializationHelper::convertMsg(inputMsg.position());
                outputMsg.orientation = subt::comms::SerializationHelper::convertMsg(inputMsg.orientation());
                return outputMsg;
            }


            static geometry_msgs::msg::PoseStamped convertMsg(const geometry_msgs::PoseStamped& inputMsg){
                geometry_msgs::msg::PoseStamped outputMsg = geometry_msgs::msg::PoseStamped();                
                outputMsg.header(subt::comms::SerializationHelper::convertMsg(inputMsg.header));
                outputMsg.pose(subt::comms::SerializationHelper::convertMsg(inputMsg.pose));
                return outputMsg;
            }

            static geometry_msgs::PoseStamped convertMsg(const geometry_msgs::msg::PoseStamped& inputMsg){
                geometry_msgs::PoseStamped outputMsg;
                outputMsg.header = subt::comms::SerializationHelper::convertMsg(inputMsg.header());
                outputMsg.pose = subt::comms::SerializationHelper::convertMsg(inputMsg.pose());
                return outputMsg;
            }


            static geometry_msgs::msg::PointStamped convertMsg(const geometry_msgs::PointStamped& inputMsg){
                geometry_msgs::msg::PointStamped outputMsg = geometry_msgs::msg::PointStamped();                
                outputMsg.header(subt::comms::SerializationHelper::convertMsg(inputMsg.header));
                outputMsg.point(subt::comms::SerializationHelper::convertMsg(inputMsg.point));
                return outputMsg;
            }

            static geometry_msgs::PointStamped convertMsg(const geometry_msgs::msg::PointStamped& inputMsg){
                geometry_msgs::PointStamped outputMsg;
                outputMsg.header = subt::comms::SerializationHelper::convertMsg(inputMsg.header());
                outputMsg.point = subt::comms::SerializationHelper::convertMsg(inputMsg.point());
                return outputMsg;
            }


            static geometry_msgs::msg::Twist convertMsg(const geometry_msgs::Twist& inputMsg){
                geometry_msgs::msg::Twist outputMsg = geometry_msgs::msg::Twist();                
                outputMsg.linear(subt::comms::SerializationHelper::convertMsg(inputMsg.linear));
                outputMsg.angular(subt::comms::SerializationHelper::convertMsg(inputMsg.angular));
                return outputMsg;
            }

            static geometry_msgs::Twist convertMsg(const geometry_msgs::msg::Twist& inputMsg){
                geometry_msgs::Twist outputMsg;
                outputMsg.linear = subt::comms::SerializationHelper::convertMsg(inputMsg.linear());
                outputMsg.angular = subt::comms::SerializationHelper::convertMsg(inputMsg.angular());
                return outputMsg;
            }

            static geometry_msgs::msg::TwistWithCovariance convertMsg(const geometry_msgs::TwistWithCovariance& inputMsg){
                geometry_msgs::msg::TwistWithCovariance outputMsg = geometry_msgs::msg::TwistWithCovariance();                
                outputMsg.twist(subt::comms::SerializationHelper::convertMsg(inputMsg.twist));
                                
                std::copy(inputMsg.covariance.begin(), inputMsg.covariance.end(), outputMsg.covariance().begin());
                
                return outputMsg;
            }

            static geometry_msgs::TwistWithCovariance convertMsg(const geometry_msgs::msg::TwistWithCovariance& inputMsg){
                geometry_msgs::TwistWithCovariance outputMsg;
                outputMsg.twist = subt::comms::SerializationHelper::convertMsg(inputMsg.twist());
                std::copy(inputMsg.covariance().begin(), inputMsg.covariance().end(), outputMsg.covariance.begin());
                // = subt::comms::SerializationHelper::convertMsg(inputMsg.point());
                
                return outputMsg;
            }


            static geometry_msgs::msg::PoseWithCovariance convertMsg(const geometry_msgs::PoseWithCovariance& inputMsg){
                geometry_msgs::msg::PoseWithCovariance outputMsg = geometry_msgs::msg::PoseWithCovariance();                
                outputMsg.pose(subt::comms::SerializationHelper::convertMsg(inputMsg.pose));
                                
                std::copy(inputMsg.covariance.begin(), inputMsg.covariance.end(), outputMsg.covariance().begin());
                
                return outputMsg;
            }

            static geometry_msgs::PoseWithCovariance convertMsg(const geometry_msgs::msg::PoseWithCovariance& inputMsg){
                geometry_msgs::PoseWithCovariance outputMsg;
                outputMsg.pose = subt::comms::SerializationHelper::convertMsg(inputMsg.pose());
                std::copy(inputMsg.covariance().begin(), inputMsg.covariance().end(), outputMsg.covariance.begin());
                // = subt::comms::SerializationHelper::convertMsg(inputMsg.point());
                
                return outputMsg;
            }


            static nav_msgs::msg::Odometry convertMsg(const nav_msgs::Odometry& inputMsg){
                nav_msgs::msg::Odometry outputMsg = nav_msgs::msg::Odometry();                
                outputMsg.header(subt::comms::SerializationHelper::convertMsg(inputMsg.header));
                outputMsg.child_frame_id(inputMsg.child_frame_id);

                outputMsg.pose(subt::comms::SerializationHelper::convertMsg(inputMsg.pose));
                return outputMsg;
            }

            static nav_msgs::Odometry convertMsg(const nav_msgs::msg::Odometry& inputMsg){
                nav_msgs::Odometry outputMsg;
                outputMsg.header = subt::comms::SerializationHelper::convertMsg(inputMsg.header());
                outputMsg.child_frame_id = inputMsg.child_frame_id();
                
                outputMsg.pose = subt::comms::SerializationHelper::convertMsg(inputMsg.pose());
                return outputMsg;
            }

            static geometry_msgs::msg::PolygonStamped convertMsg(const geometry_msgs::PolygonStamped& inputMsg){
                geometry_msgs::msg::PolygonStamped outputMsg = geometry_msgs::msg::PolygonStamped();                
                outputMsg.header(subt::comms::SerializationHelper::convertMsg(inputMsg.header));
               
                std::transform(inputMsg.polygon.points.begin(),inputMsg.polygon.points.end(),
                std::back_inserter(outputMsg.polygon().points()), [&](auto input){return subt::comms::SerializationHelper::convertMsg(input);} );
                                
                return outputMsg;
            }

            static geometry_msgs::PolygonStamped convertMsg(const geometry_msgs::msg::PolygonStamped& inputMsg){
                geometry_msgs::PolygonStamped outputMsg;
                outputMsg.header = subt::comms::SerializationHelper::convertMsg(inputMsg.header());
                std::transform(inputMsg.polygon().points().begin(),inputMsg.polygon().points().end(),
                std::back_inserter(outputMsg.polygon.points), [&](auto input){return subt::comms::SerializationHelper::convertMsg(input);} );
            
                return outputMsg;
            }

            static geometry_msgs::msg::Polygon convertMsg(const geometry_msgs::Polygon& inputMsg){
                geometry_msgs::msg::Polygon outputMsg = geometry_msgs::msg::Polygon();                               
                std::transform(inputMsg.points.begin(),inputMsg.points.end(),
                std::back_inserter(outputMsg.points()), [&](auto input){return subt::comms::SerializationHelper::convertMsg(input);} );
                                
                return outputMsg;
            }

            static geometry_msgs::Polygon convertMsg(const geometry_msgs::msg::Polygon& inputMsg){
                geometry_msgs::Polygon outputMsg;
                std::transform(inputMsg.points().begin(),inputMsg.points().end(),
                std::back_inserter(outputMsg.points), [&](auto input){return subt::comms::SerializationHelper::convertMsg(input);} );
            
                return outputMsg;
            }

            static sensor_msgs::msg::Joy convertMsg(const sensor_msgs::Joy& inputMsg){
                sensor_msgs::msg::Joy outputMsg = sensor_msgs::msg::Joy();                
                outputMsg.header(subt::comms::SerializationHelper::convertMsg(inputMsg.header));
                outputMsg.axes(inputMsg.axes);
                outputMsg.buttons(inputMsg.buttons);
              
                return outputMsg;
            }

            static sensor_msgs::Joy convertMsg(const sensor_msgs::msg::Joy& inputMsg){
                sensor_msgs::Joy outputMsg;
                outputMsg.header = subt::comms::SerializationHelper::convertMsg(inputMsg.header());
                outputMsg.axes = inputMsg.axes();
                outputMsg.buttons = inputMsg.buttons();

                return outputMsg;
            }


            static sensor_msgs::msg::Image convertMsg(const sensor_msgs::Image& inputMsg){
                sensor_msgs::msg::Image outputMsg = sensor_msgs::msg::Image();                
                outputMsg.header(subt::comms::SerializationHelper::convertMsg(inputMsg.header));
                outputMsg.height(inputMsg.height);
                outputMsg.width(inputMsg.width);
                outputMsg.is_bigendian(inputMsg.is_bigendian);
                outputMsg.step(inputMsg.step);              
                outputMsg.data(inputMsg.data);
              
                return outputMsg;
            }

            static sensor_msgs::Image convertMsg(const sensor_msgs::msg::Image& inputMsg){
                sensor_msgs::Image outputMsg;
                outputMsg.header = subt::comms::SerializationHelper::convertMsg(inputMsg.header());
                outputMsg.height = inputMsg.height();
                outputMsg.width = inputMsg.width();
                outputMsg.encoding = inputMsg.encoding();                
                outputMsg.is_bigendian = inputMsg.is_bigendian();                
                outputMsg.step = inputMsg.step();  
                outputMsg.data = inputMsg.data();

                return outputMsg;
            }

            static sensor_msgs::msg::CompressedImage convertMsg(const sensor_msgs::CompressedImage& inputMsg){
                sensor_msgs::msg::CompressedImage outputMsg = sensor_msgs::msg::CompressedImage();                
                outputMsg.header(subt::comms::SerializationHelper::convertMsg(inputMsg.header));
                outputMsg.format(inputMsg.format);
                outputMsg.data(inputMsg.data);
              
                return outputMsg;
            }

            static sensor_msgs::CompressedImage convertMsg(const sensor_msgs::msg::CompressedImage& inputMsg){
                sensor_msgs::CompressedImage outputMsg;
                outputMsg.header = subt::comms::SerializationHelper::convertMsg(inputMsg.header());
                outputMsg.format = inputMsg.format();
                outputMsg.data = inputMsg.data();

                return outputMsg;
            }


            static sensor_msgs::msg::PointCloud2 convertMsg(const sensor_msgs::PointCloud2& inputMsg){
                sensor_msgs::msg::PointCloud2 outputMsg = sensor_msgs::msg::PointCloud2();                
                outputMsg.header(subt::comms::SerializationHelper::convertMsg(inputMsg.header));
                outputMsg.height(inputMsg.height);
                outputMsg.width(inputMsg.width);
                outputMsg.is_bigendian(inputMsg.is_bigendian);
                outputMsg.point_step(inputMsg.point_step);
                outputMsg.row_step(inputMsg.row_step);
                outputMsg.is_dense(inputMsg.is_dense);

                std::transform(inputMsg.fields.begin(),inputMsg.fields.end(),
                std::back_inserter(outputMsg.fields()), [&](auto input){return subt::comms::SerializationHelper::convertMsg(input);} );

                outputMsg.data(inputMsg.data);
              
                return outputMsg;
            }

            static sensor_msgs::PointCloud2 convertMsg(const sensor_msgs::msg::PointCloud2& inputMsg){
                sensor_msgs::PointCloud2 outputMsg;
                outputMsg.header = subt::comms::SerializationHelper::convertMsg(inputMsg.header());

                outputMsg.height = inputMsg.height();
                outputMsg.width = inputMsg.width();
                outputMsg.is_bigendian = inputMsg.is_bigendian();
                outputMsg.point_step = inputMsg.point_step();
                outputMsg.row_step = inputMsg.row_step();
                outputMsg.is_dense = inputMsg.is_dense();

                std::transform(inputMsg.fields().begin(),inputMsg.fields().end(),
                std::back_inserter(outputMsg.fields), [&](auto input){return subt::comms::SerializationHelper::convertMsg(input);} );
                
                outputMsg.data = inputMsg.data();

                return outputMsg;
            }

            static visualization_msgs::msg::MarkerArray convertMsg(const visualization_msgs::MarkerArray& inputMsg){
                visualization_msgs::msg::MarkerArray outputMsg = visualization_msgs::msg::MarkerArray();                
                std::transform(inputMsg.markers.begin(),inputMsg.markers.end(),
                std::back_inserter(outputMsg.markers()), [&](auto input){return subt::comms::SerializationHelper::convertMsg(input);} );
                return outputMsg;
            }

            static visualization_msgs::MarkerArray convertMsg(const visualization_msgs::msg::MarkerArray& inputMsg){
                visualization_msgs::MarkerArray outputMsg;
                std::transform(inputMsg.markers().begin(),inputMsg.markers().end(),
                std::back_inserter(outputMsg.markers), [&](auto input){return subt::comms::SerializationHelper::convertMsg(input);} );
                return outputMsg;
            }    


            static visualization_msgs::msg::Marker convertMsg(const visualization_msgs::Marker& inputMsg){
                visualization_msgs::msg::Marker outputMsg = visualization_msgs::msg::Marker();                
                outputMsg.header(subt::comms::SerializationHelper::convertMsg(inputMsg.header));

                outputMsg.ns(inputMsg.ns);
                outputMsg.id(inputMsg.id);
                outputMsg.type(inputMsg.type);
                outputMsg.action(inputMsg.action);


                outputMsg.pose(subt::comms::SerializationHelper::convertMsg(inputMsg.pose));
                outputMsg.scale(subt::comms::SerializationHelper::convertMsg(inputMsg.scale));
                outputMsg.color(subt::comms::SerializationHelper::convertMsg(inputMsg.color));
                outputMsg.lifetime().sec(inputMsg.lifetime.sec);
                outputMsg.lifetime().nanosec(inputMsg.lifetime.nsec);

                outputMsg.frame_locked(inputMsg.frame_locked);

                std::transform(inputMsg.points.begin(),inputMsg.points.end(),
                std::back_inserter(outputMsg.points()), [&](auto input){return subt::comms::SerializationHelper::convertMsg(input);} );

                std::transform(inputMsg.colors.begin(),inputMsg.colors.end(),
                std::back_inserter(outputMsg.colors()), [&](auto input){return subt::comms::SerializationHelper::convertMsg(input);} );

                outputMsg.text(inputMsg.text);
                outputMsg.mesh_resource(inputMsg.mesh_resource);
                outputMsg.mesh_use_embedded_materials(inputMsg.mesh_use_embedded_materials);

              
                return outputMsg;
            }

            static visualization_msgs::Marker convertMsg(const visualization_msgs::msg::Marker& inputMsg){
                visualization_msgs::Marker outputMsg;
                outputMsg.header = subt::comms::SerializationHelper::convertMsg(inputMsg.header());
                outputMsg.ns= inputMsg.ns();
                outputMsg.id =inputMsg.id();
                outputMsg.type = inputMsg.type();
                outputMsg.action = inputMsg.action();

                outputMsg.pose = subt::comms::SerializationHelper::convertMsg(inputMsg.pose());
                outputMsg.scale = subt::comms::SerializationHelper::convertMsg(inputMsg.scale());
                outputMsg.color = subt::comms::SerializationHelper::convertMsg(inputMsg.color());
                outputMsg.lifetime.sec = inputMsg.lifetime().sec();
                outputMsg.lifetime.nsec = inputMsg.lifetime().nanosec();

                outputMsg.frame_locked = inputMsg.frame_locked();

                std::transform(inputMsg.points().begin(),inputMsg.points().end(),
                std::back_inserter(outputMsg.points), [&](auto input){return subt::comms::SerializationHelper::convertMsg(input);} );

                std::transform(inputMsg.colors().begin(),inputMsg.colors().end(),
                std::back_inserter(outputMsg.colors), [&](auto input){return subt::comms::SerializationHelper::convertMsg(input);} );

                outputMsg.text = inputMsg.text();
                outputMsg.mesh_resource = inputMsg.mesh_resource();
                outputMsg.mesh_use_embedded_materials = inputMsg.mesh_use_embedded_materials();

                return outputMsg;
            }
        };

    }
}



#endif
