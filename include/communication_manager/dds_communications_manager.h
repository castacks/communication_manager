#ifndef DDS_COMMS_MANAGER_H
#define DDS_COMMS_MANAGER_H

#include <base/BaseNode.h>
#include "yaml-cpp/yaml.h"
#include "ros/ros.h"
#include "std_msgs/String.h"
#include <string>
#include <regex>
#include <communication_manager/subt_comms_factory.h>
#include <communication_manager/dds_subscribe_data.h>
#include <communication_manager/dds_publish_data.h>
#include "topic_tools/shape_shifter.h"
#include "ros/message_traits.h"

namespace subt
{

namespace comms
{

class DDSCommsManager : public BaseNode
{
public:
    DDSCommsManager() : BaseNode("DDS Comms Manager"), nh("~"), participant(0), ddsPublish(participant), ddsSubscribe(participant)
    {
        this->initializeNode();
    }

    bool initialize()
    {
        return true;
    }

    bool execute()
    {
        return true;
    }

    void Run()
    {
        ros::spin();
    }

private:
    const std::string node_name = "DDS_COMMS_MANAGER";

    template <class Q>
    void populateQOS(Q&  qos, const std::string& transmission_profile){
          
           if (transmission_profile.find("reliable") != std::string::npos){
                 qos << dds::core::policy::Reliability::Reliable();
                 
                if (transmission_profile.find("last") != std::string::npos){
                 qos << dds::core::policy::History::KeepAll();
                } else {
                 qos << dds::core::policy::History::KeepLast(5);
                } 

            } else {
                 qos << dds::core::policy::Reliability::BestEffort();
            }

    }


    /**
     * @brief 
     * 
     * @tparam R 
     * @param rosTopic 
     * @param ddsTopic 
     * @param ddsToROS 
     * @param transmission 
     * @param filterCount 
     * @param control_logic 1={switchable topic}, else={legacy method}
     */
    template <class R>
    void addCallbacksForType(const std::string &rosTopic, const std::string &ddsTopic, bool ddsToROS, const std::string& transmission, uint32_t filterCount = 0, bool control_logic=false)
    {
        R tempROSType;
        auto convertedType = subt::comms::SerializationHelper::convertMsg(tempROSType);
        /// If the topic has already been created, then we just create another DataWriter or DataReader......and reuse the existing topic.
        dds::topic::Topic<decltype(convertedType)> check_topic = dds::topic::find<dds::topic::Topic<decltype(convertedType)> >(this->participant, ddsTopic);

        if (ddsToROS)
        {
            ROS_INFO_STREAM("ADD LISTENER! " << filterCount << " " << rosTopic << " " << ddsTopic);
           dds::sub::qos::DataReaderQos reader_qos = dds::core::QosProvider::Default().datareader_qos();
            populateQOS<dds::sub::qos::DataReaderQos>(reader_qos, transmission);
            if (check_topic == dds::core::null){
                this->allListeners.push_back(std::make_shared<subt::comms::SubscriberData<R, decltype(convertedType)>>(this->nh, transmission, rosTopic, ddsTopic, reader_qos, this->participant, this->ddsSubscribe));
            } else {
                this->allListeners.push_back(std::make_shared<subt::comms::SubscriberData<R, decltype(convertedType)>>(this->nh, transmission, rosTopic, ddsTopic, reader_qos, check_topic, this->participant, this->ddsSubscribe));
            }
        }
        else
        {
            ROS_INFO_STREAM("ADD PUBLISHER! " << filterCount << " " << rosTopic << " " << ddsTopic);
            dds::pub::qos::DataWriterQos writer_qos = dds::core::QosProvider::Default().datawriter_qos();
            populateQOS<dds::pub::qos::DataWriterQos>(writer_qos, transmission);
           
            if (check_topic == dds::core::null){
                this->allPublishers.push_back(std::make_shared<subt::comms::PublisherData<R, decltype(convertedType)>>(this->nh, transmission, rosTopic, ddsTopic, writer_qos, this->participant, this->ddsPublish, filterCount, control_logic));
            } else {
                this->allPublishers.push_back(std::make_shared<subt::comms::PublisherData<R, decltype(convertedType)>>(this->nh, transmission, rosTopic, ddsTopic, writer_qos, check_topic, this->participant, this->ddsPublish, filterCount));
            }
        }
    }

    template <class M>
    topic_tools::ShapeShifter::Ptr morphShape(topic_tools::ShapeShifter::Ptr &shape_shifter)
    {
        shape_shifter->morph(
            ros::message_traits::MD5Sum<M>::value(),
            ros::message_traits::DataType<M>::value(),
            ros::message_traits::Definition<M>::value(),
            "");

        return shape_shifter;
    }


    topic_tools::ShapeShifter::Ptr getROSMessageForType(const std::string &rosType)
    {

        topic_tools::ShapeShifter::Ptr shape_shifter = boost::make_shared<topic_tools::ShapeShifter>();

        if (rosType == "std_msgs::String")
        {
            std_msgs::String rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "geometry_msgs::PointStamped")
        {
            geometry_msgs::PointStamped rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "geometry_msgs::PolygonStamped")
        {
            geometry_msgs::PolygonStamped rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "geometry_msgs::Polygon")
        {
            geometry_msgs::Polygon rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "geometry_msgs::Twist")
        {
            geometry_msgs::Twist rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "std_msgs::Float")
        {
            std_msgs::Float32 rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "nav_msgs::Odometry")
        {
            nav_msgs::Odometry rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "visualization_msgs::Marker")
        {
            visualization_msgs::Marker rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "visualization_msgs::MarkerArray")
        {
            visualization_msgs::MarkerArray rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "diagnostic_msgs::DiagnosticArray")
        {
            diagnostic_msgs::DiagnosticArray rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "sensor_msgs::Joy")
        {
            sensor_msgs::Joy rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "sensor_msgs::Image")
        {
            sensor_msgs::Image rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "sensor_msgs::CompressedImage")
        {
            sensor_msgs::CompressedImage rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "std_msgs::Int32")
        {
            std_msgs::Int32 rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "std_msgs::Int32MultiArray")
        {
            std_msgs::Int32MultiArray rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "std_msgs::ByteMultiArray")
        {
            std_msgs::ByteMultiArray rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "sensor_msgs::PointCloud2")
        {
            sensor_msgs::PointCloud2 rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "std_msgs::Bool")
        {
            std_msgs::Bool rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "std_msgs::Header")
        {
            std_msgs::Header rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }
        else if (rosType == "geometry_msgs::Pose")
        {
            geometry_msgs::Pose rosMsg;
            this->morphShape<decltype(rosMsg)>(shape_shifter);
        }  
        else
        {
            throw std::runtime_error("Error: Tried to use Comms Manager with a un-registered Type |" + std::string(rosType) + "|");
        }

        return shape_shifter;
    }

    void establishTopicForType(const std::string &rosType, const std::string &rosTopic, const std::string &ddsTopic, bool ddsToROS,  const std::string& transmission, uint32_t filterCount=0, bool control_logic=false)
    {
        if (rosType == "std_msgs::String")
        {
            addCallbacksForType<std_msgs::String>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }
        else if (rosType == "geometry_msgs::PointStamped")
        {
            addCallbacksForType<geometry_msgs::PointStamped>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }
        else if (rosType == "geometry_msgs::PolygonStamped")
        {
            addCallbacksForType<geometry_msgs::PolygonStamped>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }
        else if (rosType == "geometry_msgs::Polygon")
        {
            addCallbacksForType<geometry_msgs::Polygon>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }
        else if (rosType == "geometry_msgs::Twist")
        {
            addCallbacksForType<geometry_msgs::Twist>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }
        else if (rosType == "std_msgs::Float")
        {
            addCallbacksForType<std_msgs::Float32>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }
        else if (rosType == "nav_msgs::Odometry")
        {
            addCallbacksForType<nav_msgs::Odometry>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }
        else if (rosType == "visualization_msgs::Marker")
        {
            addCallbacksForType<visualization_msgs::Marker>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }
        else if (rosType == "visualization_msgs::MarkerArray")
        {
            addCallbacksForType<visualization_msgs::MarkerArray>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }
        else if (rosType == "diagnostic_msgs::DiagnosticArray")
        {
            addCallbacksForType<diagnostic_msgs::DiagnosticArray>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }    
        else if (rosType == "sensor_msgs::Joy")
        {
            addCallbacksForType<sensor_msgs::Joy>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }
        else if (rosType == "sensor_msgs::Image")
        {
            addCallbacksForType<sensor_msgs::Image>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }
        else if (rosType == "sensor_msgs::CompressedImage")
        {
            addCallbacksForType<sensor_msgs::CompressedImage>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }
        else if (rosType == "std_msgs::Int32")
        {
            addCallbacksForType<std_msgs::Int32>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }
        else if (rosType == "std_msgs::Int32MultiArray")
        {
            addCallbacksForType<std_msgs::Int32MultiArray>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }
        else if (rosType == "std_msgs::ByteMultiArray")
        {
            addCallbacksForType<std_msgs::ByteMultiArray>(rosTopic, ddsTopic, ddsToROS, transmission, filterCount, control_logic);
        }
        else if (rosType == "sensor_msgs::PointCloud2")
        {
            addCallbacksForType<sensor_msgs::PointCloud2>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }
        else if (rosType == "std_msgs::Bool")
        {
            addCallbacksForType<std_msgs::Bool>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }
        else if (rosType == "std_msgs::Header")
        {
            addCallbacksForType<std_msgs::Header>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }
        else if (rosType == "geometry_msgs::Pose")
        {
            addCallbacksForType<geometry_msgs::Pose>(rosTopic, ddsTopic, ddsToROS, transmission,filterCount,control_logic);
        }
        else
        {
            throw std::runtime_error("Error: Tried to use Comms Manager with a un-registered Type |" + std::string(rosType) + "|");
        }
    }

    void initializeNode()
    {
        std::string configPath;

        if (!this->nh.getParam("config", configPath))
        {
            ROS_ERROR_STREAM("config_filename parameter not found");
            return;
        }

        std::string computerArg;
        if (!this->nh.getParam("computer", computerArg))
        {
            ROS_ERROR_STREAM("computer parameter not found");
            return;
        }

        ROS_INFO_STREAM("INIT NODE!!! " << computerArg << " " << configPath);

        configYAML = YAML::LoadFile(configPath);
        int select_enum = entityArray.size() - 1;
        for (; select_enum >= 0; select_enum--)
        {
            if (computerArg.find(entityArray[select_enum]) != std::string::npos)
            {
                this->entityName = entityArray[select_enum];
                ROS_INFO_STREAM("FOUND TYPE " << entityArray[select_enum]);
                break;
            }
            else if (select_enum == 0)
            {
                ROS_ERROR_STREAM("FAILED To Find type!");
                throw std::runtime_error("Bad Robot prefix!");
                return;
            }
        }
        int tempIndex = 0;

        YAML::Node sensors = configYAML["subscribe_namespace"];
        for (YAML::iterator it = sensors.begin(); it != sensors.end(); ++it)
        {
            const YAML::Node &sensor = *it;
            ROS_INFO_STREAM("ROBOT PREFIX! " << sensor.as<bool>());
            allRobotNamespace.push_back(sensor.as<bool>());
        }

        sensors = configYAML["prefixes_robots"];
        for (YAML::iterator it = sensors.begin(); it != sensors.end(); ++it)
        {
            const YAML::Node &sensor = *it;
            if (sensor.as<std::string>() == computerArg){
                ROS_INFO_STREAM("ROBOT PREFIX! " << sensor.as<std::string>());
                currentNamespace = allRobotNamespace[tempIndex];
                
            }

            allRobotPrefixes.push_back(sensor.as<std::string>());
            tempIndex++;

        }


        sensors = configYAML["topics_robots_to_basestation"];
        std::set<std::string> robot_to_base_topics; 

        for (YAML::const_iterator it = sensors.begin(); it != sensors.end(); ++it)
        {
            std::string key = it->first.as<std::string>(); // <- key
            //std::set<std::string> parsedTopics;
            YAML::Node topicData = it->second;

            ///Get the ROS type from the Strings found in the YAML.
            //auto rosType = subt::comms::TopicHelper::establishTopicForType(topicData[1].as<std::string>());
            //ROS_INFO_STREAM("GENERATING TYPE! " << topicData[1].as<std::string>());

            if (this->entityName == "base")
            {
                ///Loop on all entities....for basestation, for example
                ///DDS Topic 'ugv1_complete_cloud' --> ROS Topic /ugv1/complete_cloud
                ///DDS Topic 'ugv2_complete_cloud' --> ROS Topic /ugv2/complete_cloud
                ///DDS Topic 'uav1_complete_cloud' --> ROS Topic /uav1/complete_cloud

                ROS_INFO_STREAM("GENERATING TYPE BASE! " << topicData[1].as<std::string>() << " " << topicData[4].as<std::string>() << " " << topicData[4].as<std::string>() << " " << topicData[3].as<uint32_t>());
                // subscriberData->forwardDDSToROS<decltype(rosType)>(topicData[0].as<std::string>(), key);
                for (const std::string &rPrefix : allRobotPrefixes)
                {
                    this->establishTopicForType(topicData[1].as<std::string>().c_str(), "/" + rPrefix + topicData[0].as<std::string>(), rPrefix + "_" + key, true,topicData[4].as<std::string>(),topicData[3].as<uint32_t>());
                }
            }
            else
            {
                ///So if the computer 'name' is ugv1 then:
                /// ROS Topic 'complete_cloud' --> DDS Topic 'ugv1_complete_cloud'
                //this->addPublisher(computerArg + "_" + key, topicData[0].as<std::string>(), topicData[1].as<std::string>(), false);
                //publisherData->forwardROSToDDS<decltype(rosType)>(topicData[0].as<std::string>(), key);
                ROS_INFO_STREAM("GENERATING TYPE ROBOT! " << topicData[1].as<std::string>() << " " << topicData[4].as<std::string>() << " " << topicData[3].as<uint32_t>());
                robot_to_base_topics.insert(topicData[0].as<std::string>());
                if (!currentNamespace){
                   this->establishTopicForType(topicData[1].as<std::string>().c_str(), topicData[0].as<std::string>(), computerArg + "_" + key, false,topicData[4].as<std::string>(),topicData[3].as<uint32_t>(), topicData[2].as<bool>());
               } else {
                   this->establishTopicForType(topicData[1].as<std::string>().c_str(), "/" + computerArg + topicData[0].as<std::string>(), computerArg + "_" + key, false,topicData[4].as<std::string>(),topicData[3].as<uint32_t>(), topicData[2].as<bool>());
               }
            }
        }

        sensors = configYAML["topics_basestation_to_robots"];
        for (YAML::const_iterator it = sensors.begin(); it != sensors.end(); ++it)
        {
            std::string key = it->first.as<std::string>(); // <- key
            std::vector<std::string> parsedTopics;
            YAML::Node topicData = it->second;

            ///Get the ROS type from the Strings found in the YAML.
            ///auto rosType = subt::comms::TopicHelper::establishTopicForType(topicData[1].as<std::string>());

            if (this->entityName == "base")
            {
                ///Loop on all entities....for basestation, for example
                ///ROS Topic /ugv1/coordination_polygon --> DDS Topic 'ugv1_coordination_polygon'
                ///ROS Topic /ugv2/coordination_polygon --> DDS Topic 'ugv2_coordination_polygon'
                ///ROS Topic /uav1/coordination_polygon --> DDS Topic 'uav1_coordination_polygon'
                //this->addPublisher(key, topicData[0].as<std::string>(), topicData[1].as<std::string>(), true);
                //publisherData->forwardROSToDDS<decltype(rosType)>(topicData[0].as<std::string>(), key);

                for (const std::string &rPrefix : allRobotPrefixes)
                {
                    this->establishTopicForType(topicData[1].as<std::string>().c_str(), "/" + rPrefix + topicData[0].as<std::string>(), rPrefix + "_" + key, false,topicData[4].as<std::string>(),topicData[3].as<uint32_t>());
                }
            }
            else
            {
                ///DDS Topic 'ugv1_coordination_polygon' --> ROS Topic /coordination_polygon
                ///this->addSubscriber(computerArg + "_" +key, topicData[0].as<std::string>(), topicData[1].as<std::string>(), false);
                //subscriberData->forwardDDSToROS<decltype(rosType)>(topicData[0].as<std::string>(), key);
                this->establishTopicForType(topicData[1].as<std::string>().c_str(), topicData[0].as<std::string>(), computerArg + "_" + key, true,topicData[4].as<std::string>(),topicData[3].as<uint32_t>());
            }
        }

        sensors = configYAML["topics_robots_to_robots"];
        for (YAML::const_iterator it = sensors.begin(); it != sensors.end(); ++it)
        {
            std::string key = it->first.as<std::string>(); // <- key
            std::vector<std::string> parsedTopics;
            YAML::Node topicData = it->second;

            ///Get the ROS type from the Strings found in the YAML.
            ///auto rosType = subt::comms::TopicHelper::establishTopicForType(topicData[1].as<std::string>());

                ///Loop on all entities....for basestation, for example
                ///ROS Topic /ugv1/coordination_polygon --> DDS Topic 'ugv1_coordination_polygon'
                ///ROS Topic /ugv2/coordination_polygon --> DDS Topic 'ugv2_coordination_polygon'
                ///ROS Topic /uav1/coordination_polygon --> DDS Topic 'uav1_coordination_polygon'
                //this->addPublisher(key, topicData[0].as<std::string>(), topicData[1].as<std::string>(), true);
                //publisherData->forwardROSToDDS<decltype(rosType)>(topicData[0].as<std::string>(), key);

            if (this->entityName != "base")
            {
                ///DDS Topic 'ugv1_coordination_polygon' --> ROS Topic /coordination_polygon
                ///this->addSubscriber(computerArg + "_" +key, topicData[0].as<std::string>(), topicData[1].as<std::string>(), false);
                //subscriberData->forwardDDSToROS<decltype(rosType)>(topicData[0].as<std::string>(), key);

                ///First setup all of the topics for publishing this data to other robots....
                ///I don't think we need to do this if the same topic is already in the robot_to_basestation list....
                //this->establishTopicForType(topicData[1].as<std::string>().c_str(), topicData[0].as<std::string>(), computerArg + "_" + key, true);

                // For example when deployed on robot UGV1(entityName) and we want messages to publish messages to everyone else 
                // 
                // 
                // We will subscribe to ROS messages on local topic /integrated_to_map
                // And send over DDS but if we already set up this link on Robot to Basestation we don't need to do this.
                // 
                // ROS /integrated_to_map  -->  DDS Topic ugv1_integrated_to_map                 
                if (robot_to_base_topics.find(topicData[0].as<std::string>()) == robot_to_base_topics.end()){
                    ROS_INFO_STREAM("Publishing topic from ROS to DDS " << topicData[0].as<std::string>() << " " << computerArg + "_" + key);

                    if (!currentNamespace){
                        this->establishTopicForType(topicData[1].as<std::string>().c_str(), topicData[0].as<std::string>(), computerArg + "_" + key, false,topicData[4].as<std::string>(),topicData[3].as<uint32_t>());
                    } else {
                        this->establishTopicForType(topicData[1].as<std::string>().c_str(), "/" + computerArg + topicData[0].as<std::string>(), computerArg + "_" + key, false,topicData[4].as<std::string>(),topicData[3].as<uint32_t>());
                    }
                
                } else {
                    ROS_INFO_STREAM("Skipping topic " << topicData[0].as<std::string>() << " because we are already publishing to base.");
                }

                for (const std::string &rPrefix : allRobotPrefixes)
                {
                    if (rPrefix != computerArg){
                        ///Setup all of the topics for receiving this data from other robots....
                        ///We basically need to do this even if no one uses this data on this robot

                        // For example when deployed on robot UGV1(entityName) and we want messages from ugv2(rPrefix)
                        // 
                        // We will subscribe to ROS messages on local topic /ugv2/integrated_to_map
                        ROS_INFO_STREAM("Subscribe to remote topic " << rPrefix + "_" + key << " ---> (ROS local topic) /" << rPrefix <<topicData[0].as<std::string>());

                        // DDS Topic ugv2_integrated_to_map --> /ugv2/integrated_to_map                       
                        this->establishTopicForType(topicData[1].as<std::string>().c_str(), "/" + rPrefix + topicData[0].as<std::string>(), rPrefix + "_" + key, true,topicData[4].as<std::string>(),topicData[3].as<uint32_t>());
                    }
                }                
            }
        }

        sensors = configYAML["topics_ledger_broadcast"];
        for (YAML::const_iterator it = sensors.begin(); it != sensors.end(); ++it)
        {
            std::string key = it->first.as<std::string>(); // <- key
            std::vector<std::string> parsedTopics;
            YAML::Node topicData = it->second;

            ///Get the ROS type from the Strings found in the YAML.
            ///auto rosType = subt::comms::TopicHelper::establishTopicForType(topicData[1].as<std::string>());

                ///Loop on all entities....for basestation, for example
                ///ROS Topic /ugv1/coordination_polygon --> DDS Topic 'ugv1_coordination_polygon'
                ///ROS Topic /ugv2/coordination_polygon --> DDS Topic 'ugv2_coordination_polygon'
                ///ROS Topic /uav1/coordination_polygon --> DDS Topic 'uav1_coordination_polygon'
                //this->addPublisher(key, topicData[0].as<std::string>(), topicData[1].as<std::string>(), true);
                //publisherData->forwardROSToDDS<decltype(rosType)>(topicData[0].as<std::string>(), key);

            // if (this->entityName != "base")
            // {
                ///DDS Topic 'ugv1_coordination_polygon' --> ROS Topic /coordination_polygon
                ///this->addSubscriber(computerArg + "_" +key, topicData[0].as<std::string>(), topicData[1].as<std::string>(), false);
                //subscriberData->forwardDDSToROS<decltype(rosType)>(topicData[0].as<std::string>(), key);

                ///First setup all of the topics for publishing this data to other robots....
                ///I don't think we need to do this if the same topic is already in the robot_to_basestation list....
                //this->establishTopicForType(topicData[1].as<std::string>().c_str(), topicData[0].as<std::string>(), computerArg + "_" + key, true);

                // For example when deployed on robot UGV1(entityName) and we want messages to publish messages to everyone else 
                // 
                // 
                // We will subscribe to ROS messages on local topic /integrated_to_map
                // And send over DDS but if we already set up this link on Robot to Basestation we don't need to do this.
                // 
                // ROS /integrated_to_map  -->  DDS Topic ugv1_integrated_to_map                 
                if (robot_to_base_topics.find(topicData[0].as<std::string>()) == robot_to_base_topics.end()){
                    ROS_INFO_STREAM("Publishing topic from ROS to DDS " << topicData[0].as<std::string>() << " " << computerArg + "_" + key);

                    this->establishTopicForType(topicData[1].as<std::string>().c_str(), "/" + computerArg + topicData[0].as<std::string>(), computerArg + "_" + key, false,topicData[4].as<std::string>(),topicData[3].as<uint32_t>());
                
                } else {
                    ROS_INFO_STREAM("Skipping topic " << topicData[0].as<std::string>() << " because we are already publishing to base.");
                }

                for (const std::string &rPrefix : allRobotPrefixes)
                {
                    if (rPrefix != computerArg){
                        ///Setup all of the topics for receiving this data from other robots....
                        ///We basically need to do this even if no one uses this data on this robot

                        // For example when deployed on robot UGV1(entityName) and we want messages from ugv2(rPrefix)
                        // 
                        // We will subscribe to ROS messages on local topic /ugv2/integrated_to_map
                        ROS_INFO_STREAM("Subscribe to remote topic " << rPrefix + "_" + key << " ---> (ROS local topic) /" << rPrefix <<topicData[0].as<std::string>());

                        // DDS Topic ugv2_integrated_to_map --> /ugv2/integrated_to_map                       
                        // this->establishTopicForType(topicData[1].as<std::string>().c_str(), "/" + rPrefix + topicData[0].as<std::string>(), rPrefix + "_" + key, true,topicData[4].as<std::string>(),topicData[3].as<uint32_t>());
                        this->establishTopicForType(topicData[1].as<std::string>().c_str(), topicData[0].as<std::string>(), rPrefix + "_" + key, true,topicData[4].as<std::string>(),topicData[3].as<uint32_t>());
                
                    }
                }

                if (computerArg != "basestation") {
                  
                    this->establishTopicForType(topicData[1].as<std::string>().c_str(), topicData[0].as<std::string>(), std::string("basestation") + "_" + key, true,topicData[4].as<std::string>(),topicData[3].as<uint32_t>());
                  
                }                
            // }
        }


        sensors = configYAML["topics_ledger"];
        for (YAML::const_iterator it = sensors.begin(); it != sensors.end(); ++it)
        {
            std::string key = it->first.as<std::string>(); // <- key
            std::vector<std::string> parsedTopics;
            YAML::Node topicData = it->second;

            for (const std::string &rPrefix : allRobotPrefixes)
            {
                if (rPrefix != computerArg)
                {
                    this->establishTopicForType(topicData[1].as<std::string>().c_str(), "/" + rPrefix + topicData[0].as<std::string>(), computerArg + "_" + rPrefix + "_" + key, false,topicData[4].as<std::string>(),topicData[3].as<uint32_t>());
                    this->establishTopicForType(topicData[1].as<std::string>().c_str(), "/" + computerArg + topicData[0].as<std::string>(), rPrefix + "_" + computerArg + "_" + key, true,topicData[4].as<std::string>(),topicData[3].as<uint32_t>());
                }
            }

            if (computerArg != "basestation")
            {
                this->establishTopicForType(topicData[1].as<std::string>().c_str(), "/" + std::string("basestation") + topicData[0].as<std::string>(), computerArg + "_" + std::string("basestation") + "_" + key, false,topicData[4].as<std::string>(),topicData[3].as<uint32_t>());
                this->establishTopicForType(topicData[1].as<std::string>().c_str(), "/" + computerArg + topicData[0].as<std::string>(), std::string("basestation") + "_" + computerArg + "_" + key, true,topicData[4].as<std::string>(),topicData[3].as<uint32_t>());
            }
        }

        sensors = configYAML["topic_direct_robot"];
        for (YAML::const_iterator it = sensors.begin(); it != sensors.end(); ++it)
        {
            std::string key = it->first.as<std::string>(); // <- key

            
            std::vector<std::string> parsedTopics;
            YAML::Node topicData = it->second;
            std::string ddsTopic = key;
            std::string sourceRobot = topicData[0].as<std::string>();
            std::string destRobot = topicData[2].as<std::string>();
           
            std::string sourceROSTopic = topicData[1].as<std::string>();
            std::string destROSTopic = topicData[3].as<std::string>();
            std::string messageType = topicData[4].as<std::string>();
            uint32_t throttleAmount = topicData[5].as<uint32_t>();
            std::string transmission = topicData[6].as<std::string>();
            if (computerArg == sourceRobot){
                ROS_INFO_STREAM(ddsTopic << " Source Robot " << sourceRobot << " --> " << destRobot << " -- Topic Mapping  " << sourceROSTopic << " to " << destROSTopic << " Type= " << messageType);
                this->establishTopicForType(messageType, sourceROSTopic, ddsTopic, false, transmission, throttleAmount);  
            } else if (computerArg == destRobot) {
                ROS_INFO_STREAM(ddsTopic << " Source Robot " << sourceRobot << " --> " << destRobot << " -- Topic Mapping  " << sourceROSTopic << " to " << destROSTopic << " Type= " << messageType);
                this->establishTopicForType(messageType, destROSTopic, ddsTopic, true, transmission, throttleAmount);
            }
        }

    }

    std::string convertROSTopicToDDS(const std::string &rosTopic)
    {
        std::string ddsTopic(rosTopic);
        ROS_INFO_STREAM("Start Convert: " << ddsTopic);
        ddsTopic = std::regex_replace(ddsTopic, std::regex(std::string("/")), std::string("__"));
        ROS_INFO_STREAM("End Convert: " << ddsTopic);

        return ddsTopic;
    }

    dds::domain::DomainParticipant participant;
    ros::NodeHandle nh;
    YAML::Node configYAML;
    std::string entityName;
    std::vector<std::string> allRobotPrefixes;
    std::vector<bool> allRobotNamespace;
    bool currentNamespace;
    std::vector<std::string> baseToRobotTopics;
    std::vector<std::string> robotToBaseTopics;
    std::vector<std::any> allListeners;
    std::vector<std::any> allPublishers;

    dds::pub::Publisher ddsPublish;
    dds::sub::Subscriber ddsSubscribe;

    enum EntityType
    {
        NONE = 0,
        BASE = 1,
        UGV = 2,
        UAV = 3
    };
    const std::array<std::string, 5> entityArray{"none", "base", "uav", "drone", "drone_"};
};

} // namespace comms
} // namespace subt

#endif
