#ifndef DDS_SUBSCRIBE_DATA_H
#define DDS_SUBSCRIBE_DATA_H

#include <any>
#include "ros/ros.h"
#include <communication_manager/subt_comms_factory.h>
#include <dds/core/Time.hpp>
#include <dds/sub/SampleInfo.hpp>
#include <communication_manager/messageData.h>


namespace subt {

    namespace comms {


template <class R, class T>
class SubscriberData : public dds::sub::NoOpDataReaderListener<T>{

    public:

        SubscriberData(ros::NodeHandle& _nh, const std::string& _transmission_profile,  const std::string& _rosTopic,  const std::string& _ddsTopic, dds::sub::qos::DataReaderQos& reader_qos, dds::topic::Topic<T>& findTopic,  dds::domain::DomainParticipant& _participant, dds::sub::Subscriber& ddsSubscribe, const int queueSize=10000) : nh(_nh), rosTopic(_rosTopic), transmission_profile(_transmission_profile), ddsTopicObj(findTopic) {    
            ROS_INFO_STREAM("DDS (" << _ddsTopic << ") -> ROS ("  << rosTopic << ")");    
            //ddsTopicObj = dds::topic::find<dds::topic::Topic<T>>(_participant, ddsTopic);
            /*ROS_INFO_STREAM("DDS (" << _ddsTopic << ") -> ROS ("  << rosTopic << ")");    

            this->rosPublish = _nh.advertise<R>(rosTopic, 1000);
            this->ddsReaderObj = std::make_shared<dds::sub::DataReader<T> >(ddsSubscribe, ddsTopicObj, qos, this);      */
            this->init_data_reader(ddsSubscribe, reader_qos);
        }

        SubscriberData(ros::NodeHandle& _nh, const std::string& _transmission_profile,  const std::string& _rosTopic, const std::string& _ddsTopic, dds::sub::qos::DataReaderQos& reader_qos, dds::domain::DomainParticipant& _participant, dds::sub::Subscriber& ddsSubscribe, const int queueSize=10000) : nh(_nh), rosTopic(_rosTopic), transmission_profile(_transmission_profile), ddsTopicObj(_participant, _ddsTopic) {    

            ROS_INFO_STREAM("DDS (" << _ddsTopic << ") -> ROS ("  << rosTopic << ")");    

            /*ROS_INFO_STREAM("DDS (" << _ddsTopic << ") -> ROS ("  << rosTopic << ")");    

            this->rosPublish = _nh.advertise<R>(rosTopic, 1000);
            this->ddsReaderObj = std::make_shared<dds::sub::DataReader<T> >(ddsSubscribe, ddsTopicObj, qos, this);      */
            this->init_data_reader(ddsSubscribe, reader_qos);
        }


     virtual void on_data_available(dds::sub::DataReader<T> &reader)
    {   
        // Read the available data
        dds::sub::LoanedSamples<T> samples = reader.take();
        
        if (samples.length() > 0){
        
        //std::vector<T> data_vector;
        //std::copy(
         //   rti::sub::valid_samples(samples.begin()),
         //   rti::sub::valid_samples(samples.end()),
         //   std::back_inserter(data_vector));

        R rMsg;
        uint32_t serial_size = ros::serialization::serializationLength(rMsg);

	const std::string& topic_name = reader->topic_name();

        for (auto sample : samples){      
	    std::vector<char> msgBytes;            
	    rti::topic::to_cdr_buffer<T>(msgBytes, sample.data());
	    serial_size = msgBytes.size();
            //ROS_INFO_STREAM("DDS Received msg bytes " << msgBytes.size() << " " << serial_size << " " << rosTopic);
            /*std::stringstream ssc;
            for (int i = 0; i < msgBytes.size(); i++ ){
                ssc <<((int) msgBytes[i]) << ",";
                
            } */
            this->rosPublish.publish(subt::comms::SerializationHelper::convertMsg(sample));

	    dds::core::Time src_timestamp = sample.info().source_timestamp();
	    int32_t src_ts_s = src_timestamp.sec();
	    uint32_t src_ts_ns = src_timestamp.nanosec();
	    //double src_ts = src_timestamp.to_secs();
	    dds::core::Time rcp_timestamp = sample.info()->reception_timestamp();
	    int32_t rcp_ts_s = rcp_timestamp.sec();
	    uint32_t rcp_ts_ns = rcp_timestamp.nanosec();
	    //double rcp_ts = rcp_timestamp.to_secs();
	    
	    communication_manager::messageData msgData;
	    msgData.topic_name = topic_name;
	    msgData.source_timestamp_s = src_ts_s;
	    msgData.source_timestamp_ns = src_ts_ns;
	    msgData.reception_timestamp_s = rcp_ts_s;
	    msgData.reception_timestamp_ns = rcp_ts_ns;
	    msgData.message_size = serial_size;

	    this->msgDataPublisher.publish(msgData);
        }
        
        } else {
	  //ROS_INFO_STREAM("Zero Samples!");
        }
    }

    private: 
        void init_data_reader(dds::sub::Subscriber& ddsSubscribe, dds::sub::qos::DataReaderQos& qos){

            //qos = dds::core::QosProvider::Default().datareader_qos();
            //qos << dds::core::policy::History::KeepAll();
            /*
            if (transmission_profile.find("reliable") != std::string::npos){
                qos << dds::core::policy::Reliability::Reliable();
                //qos = dds::core::QosProvider::Default().datareader_qos();
                if (transmission_profile.find("last") != std::string::npos){
                ROS_INFO_STREAM("CREATING ROS PUBLISHER ALL!!!");

                 qos << dds::core::policy::History::KeepAll();
                } else {
                ROS_INFO_STREAM("CREATING ROS PUBLISHER LAST 5!!!");
                 qos << dds::core::policy::History::KeepLast(5);
                }
            } else {
                qos << dds::core::policy::Reliability::BestEffort();
                //qos << dds::core::policy::History::KeepLast(5);
                ROS_INFO_STREAM("CREATING ROS PUBLISHER LAST 5!!!");
           
            } */
            this->rosPublish = nh.advertise<R>(rosTopic, 1000);
            ROS_INFO_STREAM("CREATING Reader!!!");

            this->ddsReaderObj = std::make_shared<dds::sub::DataReader<T> >(ddsSubscribe, ddsTopicObj, qos, this);  

	    this->msgDataPublisher = nh.advertise<communication_manager::messageData>("/dds_message_data", 100);
         
        }
    
    std::string ddsTopic;
    std::string rosTopic;
    std::string typeCode; 
    std::string transmission_profile;
    dds::topic::Topic<T> ddsTopicObj;

    std::shared_ptr<dds::sub::DataReader<T> > ddsReaderObj;
    
    ros::Publisher rosPublish;
  
    ros::Publisher msgDataPublisher;

    bool sweepTopic;

    //This is the ID of the robot....it is hard-coded for now.
    std::string entityName;

    //This is a list of all robot prefixes.....is this even needed?
    std::vector<std::string> robotPrefixes;
  
    dds::sub::qos::DataReaderQos qos;

    YAML::Node configYAML; 
    ros::NodeHandle nh;
};
    }
}

#endif
