# Robot Prefixes
```
_______________________________________________________________
|  DRONE 1 COMPUTER                                            |
| __________________                                           |
| | Drone 1 Topics |                                           |
| |----------------|                                           |
| | /odom          |                                           |
| | /string        |                                           |
| | ...            |                                           |
| |________________|                                           |
|         |                                                    |
|         v                                                    |
| ____________________________________________________________ |
| | communication_manager publisher                          | |
| |----------------------------------------------------------| |
| | 1. /topic --> /drone_1/topic     # prefix the ROS topic  | |
| | 2. /drone_1/topic --> drone_1_topic  # ROS to DDS topic  | |
| |__________________________________________________________| |
|                                                              | 
|_________|____________________________________________________|
          |
          v

       Network
```

Topics specified in `config/params_dds.yaml` get the robot prefix prepended to them. The prefix is decided by the `computer:=prefix` roslaunch arg.

Robots see topics from other robots as `/other_robot/topic`, while its own topics are simply `/topic`.



## Custom prefixes
1) Edit `config/params_dds.yaml`, change the 
`prefixes_robots` list to what you like. For example:
    ```
    prefixes_robots: ['drone_1', 'drone_2', 'drone_3']
    ```
2) Edit `include/communication_manager/dds_communications_manager.h`:

    At the bottom of the file, edit the array that looks like
    ```
    const std::array<std::string, 4> entityArray{"none", "base", "ugv", "uav"};
    ``` 
    Add your robot prefixes here to what you want, e.g. `"drone_"`. Bump the number 4 up to however many entities there are.

3) Rebuild with catkin clean && catkin build communication_manager. Remember to resource setup.zsh.